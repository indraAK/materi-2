// Array
const numbers = new Array(1, 2, 3, 4, 5);
const fruits = ["apples", "oranges", "pears"];
fruits[3] = "grapes";
fruits.push("mangos");
fruits.unshift("strawberries");
fruits.pop();
console.log(fruits);
console.log(numbers);
console.log(fruits[0]);
console.log(Array.isArray(fruits));
console.log(fruits.indexOf("oranges"));
