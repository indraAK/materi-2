const todos = [
  {
    id: 1,
    text: "Take out trash",
    isCompleted: true,
  },
  {
    id: 2,
    text: "Meeting with boss",
    isCompleted: true,
  },
  {
    id: 3,
    text: "Dentist appt",
    isCompleted: false,
  },
];

// For
for (let i = 0; i < 10; i++) {
  console.log(`For Loop Number: ${i}`);
}

for (let i = 0; i < todos.length; i++) {
  console.log(todos[i].text);
}

for (const todo of todos) {
  console.log(todo.text);
}

// While
let i = 0;
while (i < 10) {
  console.log(`While Loop Number: ${i}`);
  i++;
}

// forEach
todos.forEach((todo) => {
  console.log(todo.text);
});

// map
const todoText = todos.map((todo) => todo.text);
console.log(todoText);

// filter
const todoCompleted = todos.filter((todo) => todo.isCompleted);
console.log(todoCompleted);
