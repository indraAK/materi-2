// Object
const person = {
  firstName: "John",
  lastName: "Doe",
  age: 30,
  hobbies: ["music", "movies", "sports"],
  address: {
    street: "50 main st",
    city: "Boston",
    state: "MA",
  },
};

person.email = "john@gmail.com";

console.log(person);
console.log(person.hobbies);
console.log(person.address);

const {
  firstName,
  lastName,
  address: { city },
} = person;
console.log(city);
