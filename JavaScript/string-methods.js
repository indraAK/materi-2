// Methods for strings
const s = "Helloi World";
console.log(s.length);
console.log(s.toLocaleLowerCase());
console.log(s.substring(0, 5));
console.log(s.substring(0, 5).toUpperCase());
console.log(s.split(""));
