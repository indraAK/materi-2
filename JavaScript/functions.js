// FUNCTIONS

// normal
function addNums(num1 = 1, num2 = 1) {
  return num1 + num2;
}
console.log(addNums(5, 5));

function greet(greeting = "Hello", name) {
  if (!name) {
    // console.log(greeting);
    return greeting;
  } else {
    // console.log(`${greeting} ${name}`);
    return `${greeting} ${name}`;
  }
}

// arrow
const greetArrow = (greeting = "Hello", name = "There") =>
  `${greeting} ${name}`;

const addNumsArrow = (num1 = 1, num2 = 1) => num1 + num2;

console.log(greet("Hi"));
console.log(greetArrow());
console.log(addNumsArrow());
